/*
 * File:   ProgrammaLED.c
 * Author: Mirko
 *
 * Created on 25 maggio 2016, 22.32
 * Basato su PIC16F628A, creato su MPLABX v3.30
 */


#include <xc.h> //importiamo le librerie per il compilatore.
#include <pic.h> //importiamo la libreria per gestire qualsiasi pic.
#include "configurazione.h" //importiamo la configurazione (conf. bits)
#define  _XTAL_FREQ 4000000 //definiamo la frequenza, necessario per il delay.



/*prima di tutto devo calcolare in base ai pin collegati al display
 * come si compongono i numeri. Ad esempio, se il pin RB1 e il pin RB2
 * illuminano rispettivamente b e c, ottengo il numero 1 impostando il
 * loro valore logico su HIGH. Quindi avendo nella PORTB 8 bit, avr�
 * PORTB=0b00000110, i  due "1" corrispondono a RB1 e RB2 in quanto parto a 
 * contare da destra. Per comodit� converto in esadecimale -> 0x06 sar� uguale
 * al numero 1.
 * La dicitura 0x indica al programma che parlo in sistema esadecimale.
 * La dicitura 0b indica al programma che parlo in sistema binario.
 * Calcolo cos� tutti i numeri e dichiaro un vettore 
 */ 


//"unsigned short int" crea una variabile di 1 byte.
unsigned short int vetNum[]={0x3F,0x06,0x5B,0x4F,0x66,0x6D,0x7D,0x07,0x7F,0x6F};
unsigned short int conta=0;


void start(){ //funzione di inizio, verr� chiamata all'inizio e quando resetto.
    conta=0; //considero la cella del vettore che corrisponde a zero (0x3F)
    TRISB=0x00; //imposto i pin della porta B tutti come output
    PORTB=0xFF;//tutti i pin della porta B (quindi collegati al display) sono HIGH per controllo.
    __delay_ms(1000); //dopo un secondo...
    PORTB=0x00; //...tornano LOW.
    CMCON = 0x07; // comparator disabilitato
    VRCON = 0x00; // modulo voltage reference disabilitato
    TRISA=0b00101110; //imposto i 3 pin A1,A2,A3 su input. A1 sar� reset, A2 sar� +, A3 sar� -
    __delay_ms(500);//aspetto ancora mezzo secondo.
}// end start

void main() {
    start(); //chiamo la funzione start
    while(1){ //imposto un loop    
        if(PORTAbits.RA2==0){ //se il pin A2 va su low cio� se premo il pulsante +    
        __delay_ms(400); //debounce grezzo
            if(PORTAbits.RA2==0){ //se il pulsante � ancora premuto dopo x ms
            conta++; // aggiunge 1 alla variabile conta, � come dire conta=conta+1
            }//end if
        }//end if
        else if(PORTAbits.RA3==0){//se il pin A3 � low cio� se premo il pulsante -
        __delay_ms(400); //debounce grezzo
            if(PORTAbits.RA3==0){//se � ancora premuto -
            conta--;//diminiusce di 1 la variabile conta.
            }//end if
        }//end else if
        else if(PORTAbits.RA1==0){//se il pin A1 � low cio� se premo il reset
            __delay_ms(400); //debounce grezzo
            if(PORTAbits.RA1==0){ //se il pin A1 � ancora low
            }//end if
        start(); //chiamo la funzione iniziale
        }//end else if
        if(conta==10) conta=0;//questo � necessario per far ricominciare da 0 dopo il 9.
        if(conta==-1) conta=9;//questo � necessario per far ricominciare da 9 dopo lo 0.
    PORTB=vetNum[conta]; /*la porta B assume il valore della cella del vettore
                      * che corrisponde alla variabile "conta". 
                      * es: se conta=0,legge la cella 0x3F -> si illumina 0.
                      *se conta=1,legge la cella 0x06 -> si illumina 1.
                      */
    }// end while
}//end main